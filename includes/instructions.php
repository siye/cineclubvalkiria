<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>VALKIRIA cineclub</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400"> <!-- Google web font "Open Sans" -->
	<link rel="stylesheet" href="../css/fontawesome-all.min.css">
	 <link rel="stylesheet" href="../css/bootstrap.min.css">
	<!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">-->
	<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">-->
	<link rel="stylesheet" type="text/css" href="../css/magnific-popup.css" />
	<link rel="stylesheet" type="text/css" href="../slick/slick.css" />
	<link rel="stylesheet" type="text/css" href="../slick/slick-theme.css" />
	<link rel="stylesheet" href="../css/tooplate-style.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/instructions.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>


	
</head>

<body>



	<div class="container-fluid tm-main">
		<div class="row tm-main-row">
			<!-- Sidebar -->
			<div id="tmSideBar" class="col-xl-3 col-lg-4 col-md-12 col-sm-12 sidebar">
				<button id="tmMainNavToggle" class="menu-icon">&#9776;</button>
				<div class="inner">
					<a href="http://cineclubvalkiria.yemafe.es/"><img class="logo mb-5" border="0" src="../img/log/viking-cinema-logo.png" /></a>
					<nav id="tmMainNav" class="tm-main-nav">
						<ul>
							<li>
								<a href="javascript:history.back()" id="tmNavLink1" class="scrolly" data-bg-img="constructive_bg_01.jpg" data-page="#tm-section-1">
									<i class="fas fa-arrow-left tm-nav-fa-icon"></i>
									<span>Volver</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 tm-content mb-5">
				<!-- section -->
				<section  class="tm-section">
					<div class="ml-auto">
						<button class="accordion text-uppercase ">¿CÓMO FUNCIONA?</button>
						<div class="my-4 panel " id="section_how_works">
							<ol>
								<li>Para unirte a nuestro cineclub debes disponer de cuenta en PLEX ya que usamos esta plataforma para compartir el contenido.</li>
								<li>Nos sincronizamos en PLEX, para esto debemos añadirnos de amigos en PLEX. Usa el formulario para hacernos llegar tu petición de acceso <a href="https://airtable.com/shrld88VfSeqoktKC" target="_blank"> (pincha aquí) </a></li>
								<li>Una vez que estemos sincronizados te daremos acceso a las colecciones, llamadas bibliotecas y ya podrás acceder al contenido!.</li>
							</ol>
						</div>
						<button class="accordion text-uppercase">¿Qué contenido voy a encontrarme?</button>
						<div class="my-4 panel">
							<p>Encontrarás más de 800 peliculas, a la máxima calidad posible ( no todo es 1080p ), series en su mayoría de ciencia ficción, terror y misterio y documentales de naturaleza.
							Lo más destacado es que encontrarás sagas completas y películas clásicas, además de novedades de cine, pero NO todo!
							Si buscas ver toda la cartelera de cine + Netflix + HBO, este NO es tu lugar.</p>
						</div>
						<button class="accordion text-uppercase">¿Aceptáis peticiones?</button>
						<div class="my-4 panel">
							<p>Claro. Dinos a traves de nuestras redes sociales cualquier serie o película que quieras ver e intentaremos que esté disponible cuanto antes.
								Todos los Miércoles (o casi todos) añadimos contenido nuevo!. </p>
								<div>
									<img src="../img/CanalTelegram.jpg" alt="QR-telegram-Valkiria-cineclub" class="qr center">
								</div>
						</div>
						<button class="accordion text-uppercase">¿Cómo me uno?</button>
						<div class="my-4 panel">
							<ol>
								<li>Para unirte a Valkiria cineclub debes rellenar el formulario <a href="https://airtable.com/shrld88VfSeqoktKC" target="_blank"> (pincha aquí) </a> si aún no estas inscrito. </li>
								<li>Ahora debes seguir el punto <a href="#section_how_works" class="scrolly" >¿cómo funciona?</a> 
									<ul>
										<li>Daremos prioridad a los miembros que colaboren activamente con donaciones.</li>
										<li>Sobretodo si eres nuevo miembro en Valkiria cineclub, recomendamos hacer alguna aportación para conseguir el acceso cuánto antes.</li>
									</ul>
								</li>
								<li>Puedes invitar a amigos o familiares. Pueden indicar que son tus referidos.</li>
							</ol>
						</div>
						<button class="accordion text-uppercase">¿Nunca he usado PLEX?</button>
						<div class="my-4 panel">
							<p>No hay problema, simplemente empieza por <a href="https://www.plex.tv/es/media-server-downloads/#plex-app" target="_blank">descargarte plex</a>  , registrate y sigue los pasos, PLEX está muy extendido y no tendrás problemas para encontrar mil videos en youtube. </p>
								
							<iframe class="center" width="560" height="315" src="https://www.youtube.com/embed/TGl_G1tXjkE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<button class="accordion text-uppercase">¿Por qué esta plataforma?</button>
						<div class="my-4 panel">
							<ul>
								<li>En Valkiria cineclub no buscamos lucrarnos, cubrimos gastos derivamos como el mantenimiento del servidor, programadores, redes sociales, etc... </li>
								<li>Las donaciones no son obligatorias pero los recursos son limitados y daremos prioridad a los miembros que apoyen de manera económica.</li>
								<li>Esta plataforma es un medio para aclarar conceptos, difundir las plataformas que usamos para mantenernos en contacto y facilitarnos la gestión de miembros y sus aportes.</li>
							</ul>
						</div>
					</div>
				</section>
			</div> <!-- .tm-content -->

			
		</div> <!-- row -->
	</div><!--end container -->
	


	<script>
		//js fo accordion
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.display === "block") {
				panel.style.display = "none";
				} else {
				panel.style.display = "block";
				}
			});
		}

		// Setup Nav Toggle Button
		function setupNavToggle() {
			$("#tmMainNavToggle").on("click", function() {
				$(".sidebar").toggleClass("show");
			});
		}
		$( document ).ready(function() {
			setupNavToggle();
		});
		
	</script>

</body>

<?php include_once('./footer.php'); ?>