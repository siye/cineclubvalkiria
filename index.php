<?php
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id = 'yourpaypalid@yahoo.com'; // Business email ID
?>


<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>VALKIRIA cineclub</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400"> <!-- Google web font "Open Sans" -->
	<!--<link rel="stylesheet" href="css/fontawesome-all.min.css">-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
	<link rel="stylesheet" type="text/css" href="slick/slick.css" />
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />
	<link rel="stylesheet" href="css/tooplate-style.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script>
		var renderPage = true;

		if (navigator.userAgent.indexOf('MSIE') !== -1 ||
			navigator.appVersion.indexOf('Trident/') > 0) {
			/* Microsoft Internet Explorer detected in. */
			alert("Please view this in a modern browser such as Chrome or Microsoft Edge.");
			renderPage = false;
		}
	</script>
</head>

<body>
	<!-- Loader -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>

	<!-- Page Content -->
	<div class="container-fluid tm-main">
		<div class="row tm-main-row">
			<!-- Sidebar -->
			<div id="tmSideBar" class="col-xl-3 col-lg-4 col-md-12 col-sm-12 sidebar">
				<button id="tmMainNavToggle" class="menu-icon">&#9776;</button>
				<div class="inner">
				<a href="http://cineclubvalkiria.yemafe.es/"><img class="logo mb-5" border="0" src="img/log/viking-cinema-logo.png" /></a>
					<nav id="tmMainNav" class="tm-main-nav">
						<ul>
							<li>
								<a href="#intro" id="tmNavLink1" class="scrolly active" data-bg-img="constructive_bg_01.jpg" data-page="#tm-section-1">
									<i class="fas fa-home tm-nav-fa-icon"></i>
									<span>Introducción</span>
								</a>
							</li>
							<!--
							<li>
								<a href="#products" id="tmNavLink2" class="scrolly" data-bg-img="constructive_bg_02.jpg" data-page="#tm-section-2" data-page-type="carousel">
								<i class="fas fa-piggy-bank tm-nav-fa-icon"></i>
									<span>Colaboración</span>
								</a>
							</li>
							-->
							<li>
								<a href="#company" class="scrolly" data-bg-img="constructive_bg_03.jpg" data-page="#tm-section-3">
									<i class="fas fa-shapes tm-nav-fa-icon"></i>
									<span>Extra</span>
								</a>
							</li>
							<li>
								<a href="#contact" class="scrolly" data-bg-img="constructive_bg_04.jpg" data-page="#tm-section-4">
									<i class="fab fa-weixin tm-nav-fa-icon"></i>
									<span>En contácto</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>

			<div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 tm-content">

				<!-- section 1 -->
				<section id="tm-section-1" class="tm-section">
					<div class="ml-auto">
						<header class="mb-4">
							<h1 class="tm-text-shadow text-center">CineClub Valkiria</h1>
						</header>
						<p class="mb-5 tm-font-big">Bienvenido a cineclub Valkiria. <br> Somos una asociación de cinéfilos que disfrutamos juntos para ver pelis, series y demás, cooperamos para acceder al contenido mediante la plataforma PLEX. <br>
							O un grupo de amigos que pasamos algunas horas delante de la caja tonta jajajaja, en todo caso siempre queda hueco para uno más así que siéntete libre de ojear todo lo que quieras y registrarte si quieres acceder.
							No buscamos lucrarnos pero todo conlleva un gasto que si somos capaces de subsanar mediante donaciones, seguro podremos estar mucho tiempo en el aire.
							<br>Gracias y sé bienvenido. 
						</p>
						<!--<a href="#" class="btn tm-btn tm-font-big" data-nav-link="#tmNavLink2">Continue...</a>
						 data-nav-link holds the ID of nav item, which means this link should behave the same as that nav item  -->
					</div>
				</section>

				<!-- section 2 -->
				<section id="tm-section-2" class="tm-section tm-section-carousel">
					<div>
						<header class="mb-4 donation-header">
							<h2 class="tm-text-shadow">Colaboraciones</h2>
							<!--	
								<form action="https://www.paypal.com/donate" method="post" target="_top" class="form-donation" >
									<input type="hidden" name="hosted_button_id" value="FEXVKEE2WJRP4" />
									<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
									<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
								</form> 
								-->
						</header>


						<div class="tm-img-container mp-4">
							<div class="tm-img-slider">

								<!--
										<form action="<?php echo $paypal_url; ?>" method="post" name="frmPayPal1">
											<input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
											<input type="hidden" name="cmd" value="_xclick">
											<input type="hidden" name="item_name" value="Donation Payment">
											<input type="hidden" name="item_number" value="1">
											<input type="hidden" name="credits" value="510">
											<input type="hidden" name="userid" value="1">
											<input type="hidden" name="amount" value="1">
											<input type="hidden" name="cpp_header_image" value="test.jpg"> <!-- Add Header Image
											<input type="hidden" name="no_shipping" value="1">
											<input type="hidden" name="currency_code" value="USD">
											<input type="hidden" name="handling" value="0">
											<input type="hidden" name="cancel_return" value="cancel.php"> <!-- Add Cancel Page url
											<input type="hidden" name="return" value="success.php"> <!-- Add Success Page url
											<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
											
										</form> 
			-->
								<!--
									<div class="tm-slider-img img-fluid box">
										<img id="customPay" alt=" " class="donation-img mb-3" >
										<h5>VIP</h5>
										<ul class="mb-1">
											<li>Todas las ventajas.</li>
											<li>Serás admirado y mimado (mayor a premium).</li>
										</ul>
										<h5 class="pricing custom-pricing">Personalizada</h5>
									</div>
								-->
								<div class="tm-slider-img img-fluid box">
									<img id="allExtras" alt=" " class="donation-img mb-3">
									<h5>Premium</h5>
									<ul class="mb-1">
										<li>Todo lo incluido en inicial y además...</li>
										<li>Acceso asegurado durante medio año.</li>
									</ul>
									<h5 class="pricing">5€</h5>
									<form action="https://www.paypal.com/donate" method="post" target="_top" class="mt-3">
										<input type="hidden" name="hosted_button_id" value="QRQLLSQ7XCY4L" />
										<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
										<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
									</form>

								</div>
								<div class="tm-slider-img img-fluid box">
									<img alt=" " id="onlyOne" class="donation-img mb-3">
									<h5>Inicial</h5>
									<ul class="mb-1">
										<li>Preferencia sobre usuarios no colaboradores. </li>
										<li>Acceso asegurado</li>
										<li>Contenido completo.</li>
										<li>Alta resolución </li>
									</ul>
									<h5 class="pricing">2€</h5>
									<!--  -->
									<form action="https://www.paypal.com/donate" method="post" target="_top" class="mt-3">
										<input type="hidden" name="hosted_button_id" value="SC2NUWRDA39LG" />
										<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
										<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
									</form>
								</div>
								<div class="tm-slider-img img-fluid box">
									<img id="basic" alt=" " class="donation-img mb-3">
									<h5>No colaborador</h5>
									<ul class="mb-1">
										<li>Acceso según capacidad</li>
									</ul>
									<h5 class="pricing">Gratis</h5>
								</div>
							</div>
						</div>
					</div>
				</section>


				<!-- section 3 -->
				<section id="tm-section-3" class="tm-section">
					<div class="row mb-4">
						<header class="col-xl-12">
							<h2 class="tm-text-shadow">Nuestro cineclub Valkiria</h2>
						</header>
					</div>
					<div class="row">
						
						<div class="col-sm-12 col-md-6 col-lg-12 col-xl-6 mb-4">
							<a href="./includes/instructions.php" >
								<div class="media tm-bg-transparent-black tm-border-white">
									<i class="fas fa-graduation-cap tm-icon-circled tm-icon-media"></i>
									<div class="media-body">
										<h3>Instrucciones / guía </h3>
										<p>¿Tienes alguna duda? aquí encontrarás respuesta a las preguntas más frecuentes.</p>
									</div>
								</div>
							</a>
						</div>
						
						<div class="col-sm-12 col-md-6 col-lg-12 col-xl-6 mb-4">
							<a href="https://airtable.com/shrld88VfSeqoktKC" target="_blank">
								<div class="media tm-bg-transparent-black tm-border-white">
									<i class="fas fa-id-card tm-icon-circled tm-icon-media"></i>
									<div class="media-body">
										<h3>Registro </h3>
										<p>Si eres nuevo deberás facilitarnos algunos datos para poder darte de alta por primera vez. Será solo 1 minuto.</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
							<a href="https://www.paypal.com/donate/?hosted_button_id=BLC7DP4FM5UA2&source=qr" target="_blank">
								<div class="media tm-bg-transparent-black tm-border-white">
									<i class="fas fa-piggy-bank tm-icon-circled tm-icon-media"></i>
									<div class="media-body">
										<h3>¿Nos ayudas? </h3>
										<p>Aceptamos donaciones, cualquier cuantía nos ayuda a mantenernos a flote un día más. Vía Paypal o tarjeta de crédito, tú decides la cantidad, GRACIAS. </p>
									</div>
								</div>
							</a>
						</div>
						<!--
						<div class="col-sm-12 col-md-6 col-lg-12 col-xl-6 mb-4">
							<a href="href=./includes/instructions.php" >
									<div class="media tm-bg-transparent-black tm-border-white">
										<i class="fas fa-thumbs-up tm-icon-circled tm-icon-media"></i>
										<div class="media-body">
											<h3>Registra tu Donativo! </h3>
											<p>Registra tu aportación si no será anónima, de cualquiera de las maneras te lo agradecemos</p>
										</div>
									</div>
								</a>
							</div>
						</div>
						-->
				</section>

				<!-- section 4 -->
				<section id="tm-section-4" class="tm-section">
					<div class="tm-bg-transparent-black tm-contact-box-pad">
						<div class="row mb-4">
							<div class="col-sm-12">
								<header>
									<h2 class="tm-text-shadow">Estamos en contacto!</h2>
								</header>
							</div>
						</div>
						<div class="row tm-page-4-content">
							<div class="col-md-12 col-sm-12 tm-contact-col">
								<!--
								<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
									<a href="https://www.paypal.com/donate/?hosted_button_id=BLC7DP4FM5UA2&source=qr" target="_blank">
										<div class="media tm-bg-transparent-black tm-border-white">
											<i class="fas fa-piggy-bank tm-icon-circled tm-icon-media"></i>
											<div class="media-body">
												<h3>Donaciones </h3>
												<p>Como dice el refrán "<i>Es de bien nacido ser agradecido</i>". Usaremos esos fondos para cubrir gastos, MIL GRACIAS!!  </p>
												<img  class="qr center" src="img/FreeDonation.png" alt="QR-free-donation-Valkiria-cineclub" class="qr center">
											</div>
										</div>
									</a>
								</div>
									-->
								<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
									<div class="media tm-bg-transparent-black tm-border-white">
										<i class="fas fa-share-alt tm-icon-circled tm-icon-media"></i>
										<div class="media-body ">
											<h3>Compártenos </h3>
											<p>Siéntete libre de hablar de cine club Valkiria con tus compañeros, amigos y familiares si también son amantes del cine, sólo tardarán 1 min en el registro </p>
											<div class="shareBox mt-3" >
												<!--
												<a href="https://t.me/share/url?url=https://airtable.com/shrld88VfSeqoktKC&text=Registro-en-CineClub-Valkiria" target="_blank">
													<i class="fab fa-telegram fa-1x telegramColour" aria-hidden="true"></i> Compartir por Telegram 
												</a> -->
												<a class="tlgrm  mb-2 " href="https://t.me/share/url?url=http://cineclubvalkiria.yemafe.es/" target="_blank">
													<i class="fab fa-telegram fa-1x telegramColour pr-1" aria-hidden="true"></i> Compartir por Telegram 
												</a> 						
												<!--
												<a href=
												"whatsapp://send?text=https%3A%2F%2Fairtable.com%2Fshrld88VfSeqoktKC"
													data-action="share/whatsapp/share"
													target="_blank">
													Compartir en whatsapp
												</a>
												-->
												<a class="whtspp" href=
													"whatsapp://send?text=http%3A%2F%2Fcineclubvalkiria.yemafe.es%2F"
													data-action="share/whatsapp/share"
													target="_blank">
													<i class="fab fa-whatsapp fa-1x whatsappColour pr-1" aria-hidden="true"></i>
													Compartir por whatsapp
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 tm-contact-col">
								<div class="tm-address-box">
									<div class="mb-5">
										<p><i class="fab fa-facebook fa-3x facebookColour"></i> <a href="https://www.facebook.com/groups/valkiriacineclub/" target="_blank">Síguenos en Facebook  </a></p>
										<img  class="qr center " src="img/FacebookGroup_ValkiriaCineClub_qr_img.png" alt="QR-telegram-Valkiria-cineclub" class="qr center">
									</div>
									
									<div>
										<p> <i class="fab fa-telegram fa-3x telegramColour" aria-hidden="true"></i><a href="https://t.me/cineclubValkiria" target="_blank">  Únete a Telegram </a> </p>
										<img  class="qr center" src="img/CanalTelegram.jpg" alt="QR-telegram-Valkiria-cineclub" class="qr center">
									</div>
								</div>
							</div>
									
						</div>
					</div>
				</section>
			</div> <!-- .tm-content -->
			
			<?php include_once('includes/footer.php'); ?>
		</div> <!-- row -->
	</div>
	<div id="preload-01"></div>
	<div id="preload-02"></div>
	<div id="preload-03"></div>
	<div id="preload-04"></div>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="slick/slick.min.js"></script> <!-- Slick Carousel -->

	<script>
		var sidebarVisible = false;
		var currentPageID = "#tm-section-1";
		
		// Setup Carousel
		function setupCarousel() {

			// If current page isn't Carousel page, don't do anything.
			if ($('#tm-section-2').css('display') == "none") {} else { // If current page is Carousel page, set up the Carousel.

				var slider = $('.tm-img-slider');
				var windowWidth = $(window).width();

				if (slider.hasClass('slick-initialized')) {
					slider.slick('destroy');
				}

				if (windowWidth < 640) {
					slider.slick({
						dots: true,
						infinite: false,
						slidesToShow: 1,
						slidesToScroll: 1
					});
				} else if (windowWidth < 992) {
					slider.slick({
						dots: true,
						infinite: false,
						slidesToShow: 2,
						slidesToScroll: 1
					});
				} else {
					// Slick carousel
					slider.slick({
						dots: true,
						infinite: false,
						slidesToShow: 3,
						slidesToScroll: 2
					});
				}

				// Init Magnific Popup
				$('.tm-img-slider').magnificPopup({
					delegate: 'a', // child items selector, by clicking on it popup will open
					type: 'image',
					gallery: {
						enabled: true
					}
					// other options
				});
			}
		}
		
		// Setup Nav
		function setupNav() {
			// Add Event Listener to each Nav item
			$(".tm-main-nav a").click(function(e) {
				e.preventDefault();

				var currentNavItem = $(this);
				changePage(currentNavItem);

				setupCarousel();
				setupFooter();

				// Hide the nav on mobile
				$("#tmSideBar").removeClass("show");
			});
		}

		function changePage(currentNavItem) {
			// Update Nav items
			$(".tm-main-nav a").removeClass("active");
			currentNavItem.addClass("active");

			$(currentPageID).hide();

			// Show current page
			currentPageID = currentNavItem.data("page");
			$(currentPageID).fadeIn(1000);

			// Change background image
			var bgImg = currentNavItem.data("bgImg");
			$.backstretch("img/" + bgImg);
		}

		// Setup Nav Toggle Button
		function setupNavToggle() {

			$("#tmMainNavToggle").on("click", function() {
				$(".sidebar").toggleClass("show");
			});
		}

		// If there is enough room, stick the footer at the bottom of page content.
		// If not, place it after the page content
		function setupFooter() {

			var padding = 100;
			var footerPadding = 40;
			var mainContent = $("section" + currentPageID);
			var mainContentHeight = mainContent.outerHeight(true);
			var footer = $(".footer-link");
			var footerHeight = footer.outerHeight(true);
			var totalPageHeight = mainContentHeight + footerHeight + footerPadding + padding;
			var windowHeight = $(window).height();

			if (totalPageHeight > windowHeight) {
				$(".tm-content").css("margin-bottom", footerHeight + footerPadding + "px");
				footer.css("bottom", footerHeight + "px");
			} else {
				$(".tm-content").css("margin-bottom", "0");
				footer.css("bottom", "20px");
			}
		}

		// Everything is loaded including images.
		$(window).on("load", function() {

			// Render the page on modern browser only.
			if (renderPage) {
				// Remove loader
				$('body').addClass('loaded');

				// Page transition
				var allPages = $(".tm-section");

				// Handle click of "Continue", which changes to next page
				// The link contains data-nav-link attribute, which holds the nav item ID
				// Nav item ID is then used to access and trigger click on the corresponding nav item
				var linkToAnotherPage = $("a.tm-btn[data-nav-link]");

				if (linkToAnotherPage != null) {

					linkToAnotherPage.on("click", function() {
						var navItemToHighlight = linkToAnotherPage.data("navLink");
						$("a" + navItemToHighlight).click();
					});
				}

				// Hide all pages
				allPages.hide();

				$("#tm-section-1").fadeIn();

				// Set up background first page
				var bgImg = $("#tmNavLink1").data("bgImg");

				$.backstretch("img/" + bgImg, {
					fade: 500
				});

				// Setup Carousel, Nav, and Nav Toggle
				setupCarousel();
				setupNav();
				setupNavToggle();
				setupFooter();

				// Resize Carousel upon window resize
				$(window).resize(function() {
					setupCarousel();
					setupFooter();
				});
			}
		});
	</script>
</body>

</html>